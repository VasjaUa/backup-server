package com.plexteq.restbackupservice.dto.resp;

import java.io.Serializable;

public class PostBackupsResp implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long backupId;

    public PostBackupsResp(Long backupId) {
        this.backupId = backupId;
    }
}
