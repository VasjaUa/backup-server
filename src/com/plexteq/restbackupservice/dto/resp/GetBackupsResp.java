package com.plexteq.restbackupservice.dto.resp;

import java.io.Serializable;
import java.util.Date;

public class GetBackupsResp implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long backupId;
    private Date date;
    private String status;

    public Long getBackupId() {
        return backupId;
    }
    public void setBackupId(Long backupId) {
        this.backupId = backupId;
    }

    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
}