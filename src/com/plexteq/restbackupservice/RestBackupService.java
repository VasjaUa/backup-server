package com.plexteq.restbackupservice;

import com.plexteq.restbackupservice.backupexecutor.BackupAsyncExecutor;
import com.plexteq.cnfg.ConfigParams;
import com.sun.net.httpserver.HttpServer;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.InetSocketAddress;

public enum RestBackupService {
    INSTANCE;

    private final Logger log = Logger.getLogger(RestBackupService.class);

    private BackupAsyncExecutor backupAsyncExecutor;
    private HttpServer httpServer;
    private boolean wasCreated = false;

    RestBackupService() {
        try {
            this.httpServer = HttpServer.create();
            this.wasCreated = true;
        } catch (IOException ex) {
            log.error("", ex);
        }
    }

    public boolean wasCreated() {
        return wasCreated;
    }

    public boolean init(ConfigParams cnfgParams) {
        try {
            backupAsyncExecutor = new BackupAsyncExecutor(cnfgParams);

            final int port = cnfgParams.getHttpPort();
            final String todoBaseURI = cnfgParams.getServerTodoBaseURI();
            httpServer.bind(new InetSocketAddress(port), 0);
            httpServer.createContext("/", new RestBackupServiceHandler(backupAsyncExecutor, todoBaseURI));
        } catch (IOException ex) {
            log.error("Bind InetSocketAddress.", ex);
            return false;
        }

        return true;
    }

    public void run() {
        backupAsyncExecutor.run();
        httpServer.setExecutor(null);
        httpServer.start();
    }

    public void stop() {
        backupAsyncExecutor.stop();
        httpServer.stop(0);
    }

}
