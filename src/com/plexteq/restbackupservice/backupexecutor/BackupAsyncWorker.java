package com.plexteq.restbackupservice.backupexecutor;

import com.plexteq.backupdb.BackupDB;
import com.plexteq.restbackupservice.dto.todo.Todo;
import com.plexteq.restbackupservice.dto.todo.User;
import org.apache.log4j.Logger;

import java.util.List;

public class BackupAsyncWorker implements Runnable {
    private static final Logger log = Logger.getLogger(BackupAsyncWorker.class);

    private final Long backupId;
    private final List<User> users;

    public BackupAsyncWorker(Long backupId, List<User> users) {
        this.backupId = backupId;
        this.users = users;
    }

    @Override
    public void run() {
        StringBuilder builder = new StringBuilder();
        try {
            for (User user: users) {
                builder.append(user.getUserName()).append(";");
                final List<Todo> todos = user.getTodos();
                for (Todo todo: todos) {
                    builder.append(todo.getId()).append(";")
                            .append(todo.getSubject()).append(";")
                            .append(todo.getDate()).append(";")
                            .append(todo.getDone()).append("\n");
                }
            }
            BackupDB.setBackupInfoData(backupId, builder.toString());
            BackupDB.setBackupStatus_OK(backupId);
            log.debug("Set backup status OK for backupId = " + backupId + ".");
        } catch (Exception ex) {
            BackupDB.setBackupStatus_FAILED(backupId);
            log.debug("Set backup status FAILED for backupId = " + backupId + ".");
        }
    }
}