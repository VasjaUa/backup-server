package com.plexteq.restbackupservice.backupexecutor;

import com.plexteq.cnfg.ConfigParams;
import org.apache.log4j.Logger;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class BackupAsyncExecutor {
    private static final Logger log = Logger.getLogger(BackupAsyncExecutor.class);

    private ConfigParams cnfgParams;
    private ExecutorService execService;

    public BackupAsyncExecutor(ConfigParams cnfgParams) {
        this.cnfgParams = cnfgParams;
    }

    public void execute(Runnable asyncWorker) {
        execService.execute(asyncWorker);
    }

    public void run() {
        execService = getExecutorService();
        log.debug("BackupAsyncExecutor was ran.");
    }

    public void stop() {
        if (execService != null) {
            execService.shutdown();
        }
        log.debug("BackupAsyncExecutor was stopped.");
    }

    private ExecutorService getExecutorService() {
        boolean isEnabledCTP = cnfgParams.isEnabledCachedThreadPool();
        boolean isEnabledFTP = cnfgParams.isEnabledFixedThreadPool();
        int nThreadFTP = cnfgParams.getCountThread4FixedThreadPool();

        log.debug("backupAsyncExecutor.cachedThreadPool.isEnabled = " + isEnabledCTP);
        log.debug("backupAsyncExecutor.fixedThreadPool.isEnabled = " + isEnabledFTP);
        log.debug("backupAsyncExecutor.fixedThreadPool.nThread = " + nThreadFTP);

        return logicSelectExecutorService(isEnabledCTP, isEnabledFTP, nThreadFTP);
    }

    private ExecutorService logicSelectExecutorService(boolean isEnabledCTP, boolean isEnabledFTP, int nThreadFTP) {
        if ( (isEnabledCTP == isEnabledFTP) || (isEnabledCTP && !isEnabledFTP) ) {
            log.debug("NewCachedThreadPool was selected as Executor.");
            return Executors.newCachedThreadPool();
        } else {
            log.debug("NewFixedThreadPool was selected as Executor.");
            return Executors.newFixedThreadPool(nThreadFTP);
        }
    }
}