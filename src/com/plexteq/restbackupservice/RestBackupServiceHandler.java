package com.plexteq.restbackupservice;

import com.plexteq.backupdb.BackupDB;
import com.plexteq.restbackupservice.backupexecutor.BackupAsyncExecutor;
import com.plexteq.restbackupservice.backupexecutor.BackupAsyncWorker;
import com.plexteq.restbackupservice.dto.resp.GetBackupsResp;
import com.plexteq.restbackupservice.dto.resp.PostBackupsResp;
import com.plexteq.restbackupservice.dto.todo.User;
import com.plexteq.restbackupservice.exception.FaviconIcoException;
import com.plexteq.restbackupservice.exception.IncorrectUrlException;
import com.plexteq.restbackupservice.exception.IncorrectUrlParamException;
import com.plexteq.restclient.RestClient;
import com.plexteq.util.Utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpStatus;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.net.URI;
import java.util.List;

public class RestBackupServiceHandler implements HttpHandler {
    private static final Logger log = Logger.getLogger(RestBackupServiceHandler.class);

    private static final String MIME_TYPE_APPLICATION_JSON = "application/json";
    private static final String MIME_TYPE_TEXT_CSV = "text/csv";

    private static final String HTTP_METHOD_GET = "GET";
    private static final String HTTP_METHOD_POST = "POST";

    private final BackupAsyncExecutor backupAsyncExecutor;
    private final RestClient restClient;

    public RestBackupServiceHandler(BackupAsyncExecutor backupAsyncExecutor, String baseURI) {
        this.backupAsyncExecutor = backupAsyncExecutor;
        this.restClient = new RestClient(baseURI);
    }

    @Override
    public void handle(HttpExchange httpExchange) throws IOException {
        String requestMethod = httpExchange.getRequestMethod();
        try {
            switch (requestMethod) {
                case HTTP_METHOD_GET:
                    handleGetRequest(httpExchange);
                    break;
                case HTTP_METHOD_POST:
                    handlePostRequest(httpExchange);
                    break;
                default:
                    Utils.sendResponse(httpExchange, HttpStatus.SC_METHOD_NOT_ALLOWED, null);
            }
        } catch (FaviconIcoException ex) {
            Utils.sendResponse(httpExchange, HttpStatus.SC_OK, null);
        } catch (IncorrectUrlParamException | IncorrectUrlException ex) {
            Utils.sendResponse(httpExchange, HttpStatus.SC_BAD_REQUEST, null);
        }
    }

    private void handlePostRequest(HttpExchange httpExchange) {
        String path = httpExchange.getRequestURI().getPath();
        switch (path) {
            case "/backups":
                handlePostBackups(httpExchange);
                break;
            default:
                throw new IncorrectUrlException();
        }
    }

    private void handlePostBackups(HttpExchange httpExchange) {
        final Long backupId = BackupDB.init();

        try {
            List<User> users = restClient.getUsers();
            backupAsyncExecutor.execute(new BackupAsyncWorker(backupId, users));
        } catch (IOException ex) {
            BackupDB.setBackupStatus_FAILED(backupId);
        }

        httpExchange.getResponseHeaders().set(HttpHeaders.CONTENT_TYPE, MIME_TYPE_APPLICATION_JSON);
        final byte jsonBytes[] = new Gson().toJson(new PostBackupsResp(backupId)).getBytes();
        Utils.sendResponse(httpExchange, HttpStatus.SC_OK, jsonBytes);
    }

    /************************
     * /path/{id}
     * subPaths[0] = ""
     * subPaths[1] = path
     * subPath[2] = {id}
     * subPaths.length = 3
     ***********************/
    private void handleGetRequest(final HttpExchange httpExchange) {
        final URI uri = httpExchange.getRequestURI();
        final String path = uri.getPath();

        final String[] subPaths = path.split("/");

        switch (subPaths[1]) {
            case "favicon.ico":
                throw new FaviconIcoException();

            case "backups":
                handleGetBackups(httpExchange);
                break;

            case "exports":
                long id = -1;
                if ( subPaths.length == 3 && (id = Utils.convertStringToLong(subPaths[2])) != -1 ) {
                    handleGetExportsByBackupId(httpExchange, id);
                } else {
                    throw new IncorrectUrlParamException();
                }
                break;

            default:
                throw new IncorrectUrlException();
        }
    }

    private void handleGetBackups(final HttpExchange httpExchange) {
        List<GetBackupsResp> responseBody = BackupDB.getResponseBody4RequestGetBackups();
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        final byte[] jsonBytes = gson.toJson(responseBody).getBytes();
        httpExchange.getResponseHeaders().set(HttpHeaders.CONTENT_TYPE, MIME_TYPE_APPLICATION_JSON);
        Utils.sendResponse(httpExchange, HttpStatus.SC_OK, jsonBytes);
    }

    private void handleGetExportsByBackupId(HttpExchange httpExchange, long backupId) {
        String responseBody = BackupDB.getResponseBody4RequestGetExportsById(backupId);
        if (responseBody == null) { throw new IncorrectUrlParamException(); }
        byte[] csvBytes = responseBody.getBytes();
        httpExchange.getResponseHeaders().set(HttpHeaders.CONTENT_TYPE, MIME_TYPE_TEXT_CSV);
        Utils.sendResponse(httpExchange, HttpStatus.SC_OK, csvBytes);
    }
}