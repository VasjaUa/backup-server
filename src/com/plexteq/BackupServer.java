package com.plexteq;

import com.plexteq.backupdb.BackupDB;
import com.plexteq.cnfg.ConfigParams;
import com.plexteq.restbackupservice.RestBackupService;
import org.apache.log4j.Logger;

import java.io.IOException;

public class BackupServer {
    private static final Logger log = Logger.getLogger(BackupServer.class);
    private static final String CONFIG_FILE = "config.properties";

    private static ConfigParams cnfgParams;

    private BackupServer(){}

    public static void main(String[] args) {
        if (!BackupServer.init()) {
            log.error("Can't init BackupServer.");
            return;
        }

        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() { BackupServer.stop(); }
        });

        log.info("Press CTRL+C to stop BackupServer...");
        BackupServer.run();
    }

    public static boolean init() {
        if (!readConfigParams()) {
            log.error("Can't read config file with properties: " + CONFIG_FILE + ".");
            return false;
        }

        if (!checkRestBackupServiceWasCreated()) {
            log.error("HTTP RestBackupService wasn't created.");
            return false;
        }

        if (!RestBackupService.INSTANCE.init(cnfgParams)) {
            log.error("HTTP RestBackupService wasn't initiated");
            return false;
        }

        BackupDB.initConcurrentHashMap(cnfgParams);

        return true;
    }

    public static void run() {
        RestBackupService.INSTANCE.run();
    }

    public static void stop() {
        RestBackupService.INSTANCE.stop();
    }

    private static boolean checkRestBackupServiceWasCreated() {
        return RestBackupService.INSTANCE.wasCreated() ? true : false;
    }

    private static boolean readConfigParams() {
        try {
            cnfgParams = new ConfigParams(CONFIG_FILE);
        } catch (IOException ex) {
            return false;
        }
        return true;
    }
}