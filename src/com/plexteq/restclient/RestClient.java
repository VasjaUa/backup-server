package com.plexteq.restclient;

import com.plexteq.restbackupservice.dto.todo.User;
import com.google.gson.Gson;
import org.apache.http.client.fluent.Request;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class RestClient {
    private static final Logger log = Logger.getLogger(RestClient.class);

    private final String baseURI;

    public RestClient(String baseURI) {
        this.baseURI = baseURI;
    }

    public List<User> getUsers() throws IOException {
        String uri = baseURI + "users";
        log.debug("URI: " + uri);
        String jsonBody = Request.Get(uri).execute().returnContent().asString();
        log.debug("JSON: " + jsonBody);
        return Arrays.asList(new Gson().fromJson(jsonBody, User[].class));
    }
}