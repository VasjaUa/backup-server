package com.plexteq.backupdb;

import com.plexteq.cnfg.ConfigParams;
import com.plexteq.restbackupservice.dto.resp.GetBackupsResp;
import com.plexteq.util.Utils;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class BackupDB {
    private static final Logger log = Logger.getLogger(BackupDB.class);
    private static final AtomicLong ID = new AtomicLong(0);
    private static Map<Long, BackupInfo> mapTodos;

    private BackupDB() {}

    public static void initConcurrentHashMap(final ConfigParams cnfgParams) {
        final int nInitialCapacity = cnfgParams.getInitialCapacity4ConcurrentHashMap();
        final float fLoadFactory = cnfgParams.getLoadFactory4ConcurrentHashMap();
        final int nConcurrencyLevel = cnfgParams.getConcurrencyLevel4ConcurrentHashMap();
        log.debug("[ nInitialCapacity = " + nInitialCapacity + ", fLoadFactory = " + fLoadFactory + ", nConcurrencyLevel = " + nConcurrencyLevel + " ]");

        mapTodos = new ConcurrentHashMap<>(nInitialCapacity, fLoadFactory, nConcurrencyLevel);
    }

    public static Long init() {
        Long backupId = ID.incrementAndGet();
        log.debug("Generate new backupId = " + backupId);

        BackupInfo info = new BackupInfo();
        info.setBackupId(backupId);
        info.setDate(Utils.getCurrentDate());
        info.setStatus(BackupStatus.IN_PROGRESS);

        mapTodos.put(backupId, info);

        return backupId;
    }

    public static void setBackupStatus_OK(Long id) {
        mapTodos.get(id).setStatus(BackupStatus.OK);
    }
    public static void setBackupStatus_FAILED(Long id) {
        mapTodos.get(id).setStatus(BackupStatus.FAILED);
    }

    public static void setBackupInfoData(Long id, String data) {
        mapTodos.get(id).setData(data);
    }

    public static List<GetBackupsResp> getResponseBody4RequestGetBackups() {
        List<GetBackupsResp> responseBody = new ArrayList<>(mapTodos.size());

        for (Long backupId: mapTodos.keySet()) {
            BackupInfo backupInfo = mapTodos.get(backupId);
            GetBackupsResp backupsResp = new GetBackupsResp();
            backupsResp.setBackupId(backupId);
            backupsResp.setDate(backupInfo.getDate());
            backupsResp.setStatus(backupInfo.getStatus().toString());

            responseBody.add(backupsResp);
        }
        return responseBody;
    }

    public static String getResponseBody4RequestGetExportsById(Long backupId) {
        BackupInfo backupInfo = mapTodos.get(backupId);
        StringBuilder builder = new StringBuilder("Username;TodoItemId;Subject;DueDate;Done");
        return backupInfo == null ? null : builder.append(backupInfo.getData()).toString();
    }
}