package com.plexteq.backupdb;

public enum BackupStatus {
    IN_PROGRESS,
    OK,
    FAILED
}
