package com.plexteq.backupdb;

import java.io.Serializable;
import java.util.Date;

public class BackupInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    private Long backupId;
    private Date date;
    private BackupStatus status;
    private String data;

    public Long getBackupId() {
        return backupId;
    }
    public void setBackupId(Long backupId) {
        this.backupId = backupId;
    }

    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }

    public BackupStatus getStatus() {
        return status;
    }
    public void setStatus(BackupStatus status) {
        this.status = status;
    }

    public String getData() {
        return data;
    }
    public void setData(String data) {
        this.data = data;
    }
}