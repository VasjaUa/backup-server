package com.plexteq.util;

import com.sun.net.httpserver.HttpExchange;
import org.apache.log4j.Logger;

import java.io.IOException;
import java.io.OutputStream;
import java.time.Instant;
import java.util.Date;

public class Utils {
    private static final Logger log = Logger.getLogger(Utils.class);

    private Utils() {}

    public static long convertStringToLong(String param) {
        long lResult = -1;
        try {
            lResult = Long.parseLong(param);
        } catch (NumberFormatException ex) {
            log.error("NumberFormatException for parameter: " + param + ".");
        }
        return lResult;
    }

    public static Date getCurrentDate() {
        return Date.from(Instant.now());
    }

    public static void sendResponse(final HttpExchange httpExchange, final int httpStatus, final byte[] bytes) {
        OutputStream outputStream = null;
        try {
            if (bytes != null) {
                httpExchange.sendResponseHeaders(httpStatus, bytes.length);
                outputStream = httpExchange.getResponseBody();
                outputStream.write(bytes);
            } else {
                httpExchange.sendResponseHeaders(httpStatus, -1);
            }
            log.debug("Send response with HTTP_STATUS: [" + httpStatus + "] on REMOTE_IP_ADDRESS: [" + httpExchange.getRemoteAddress() + "].");
        } catch (IOException e) {
            log.error("Write bytes by output stream.");
        } finally {
            if (outputStream != null) {
                try {
                    outputStream.close();
                } catch (IOException ex) {
                    log.error("Close output stream.");
                }
            }
        }
    }
}