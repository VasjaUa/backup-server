package com.plexteq.cnfg;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigParams {
    private static final String PROP_PORT = "server.port";
    private static final String DEF_VALUE_PORT = "80";

    private static final String PROP_SERVER_TODO_BASEURI = "serverTODO.baseURI";
    private static final String DEF_VALUE_SERVER_TODO_BASEURI = "http://localhost:9000/";

    private static final String PROP_BAE_CACHEDTP_IS_ENABLED = "backupAsyncExecutor.cachedThreadPool.isEnabled";
    private static final String DEF_VALUE_BAE_CACHEDTP_IS_ENABLED = "true";

    private static final String PROP_BAE_FIXEDTP_IS_ENABLED = "backupAsyncExecutor.fixedThreadPool.isEnabled";
    private static final String DEF_VALUE_BAE_FIXEDTP_IS_ENABLED = "false";

    private static final String PROP_BAE_FIXEDTP_COUNT_THREAD = "backupAsyncExecutor.fixedThreadPool.nThread";
    private static final String DEF_VALUE_BAE_FIXEDTP_COUNT_THREAD = "10";

    private static final String PROP_CHM_INITIAL_CAPACITY = "concurrentHashMap.initialCapacity";
    private static final String DEF_VALUE_CHM_INITIAL_CAPACITY = "16";

    private static final String PROP_CHM_LOAD_FACTOR = "concurrentHashMap.loadFactor";
    private static final String DEF_VALUE_CHM_LOAD_FACTOR = "0.75";

    private static final String PROP_CHM_CONCURRENCY_LEVEL = "concurrentHashMap.concurrencyLevel";
    private static final String DEF_VALUE_CHM_CONCURRENCY_LEVEL = "16";

    private Properties prop = new Properties();

    public ConfigParams(final String configFile) throws IOException {
        try (InputStream inConfigFile = new FileInputStream(configFile)) {
            prop.load(inConfigFile);
        }
    }

    public int getHttpPort() {
        int port;
        try {
            port = Integer.parseInt(prop.getProperty(PROP_PORT, DEF_VALUE_PORT));
        } catch (NumberFormatException ex) {
            port = Integer.parseInt(DEF_VALUE_PORT);
        }
        return port;
    }

    public String getServerTodoBaseURI() {
        return prop.getProperty(PROP_SERVER_TODO_BASEURI, DEF_VALUE_SERVER_TODO_BASEURI);
    }

    public boolean isEnabledCachedThreadPool() {
        return Boolean.parseBoolean(prop.getProperty(PROP_BAE_CACHEDTP_IS_ENABLED, DEF_VALUE_BAE_CACHEDTP_IS_ENABLED));
    }

    public boolean isEnabledFixedThreadPool() {
        return Boolean.parseBoolean(prop.getProperty(PROP_BAE_FIXEDTP_IS_ENABLED, DEF_VALUE_BAE_FIXEDTP_IS_ENABLED));
    }

    public int getCountThread4FixedThreadPool() {
        int nThread;
        try {
            nThread = Integer.parseInt(prop.getProperty(PROP_BAE_FIXEDTP_COUNT_THREAD, DEF_VALUE_BAE_FIXEDTP_COUNT_THREAD));
        } catch (NumberFormatException ex) {
            nThread = Integer.parseInt(DEF_VALUE_BAE_FIXEDTP_COUNT_THREAD);
        }
        return nThread;
    }

    public int getInitialCapacity4ConcurrentHashMap() {
        int nInitialCapacity;
        try {
            nInitialCapacity = Integer.parseInt(prop.getProperty(PROP_CHM_INITIAL_CAPACITY, DEF_VALUE_CHM_INITIAL_CAPACITY));
        } catch (NumberFormatException ex) {
            nInitialCapacity = Integer.parseInt(DEF_VALUE_CHM_INITIAL_CAPACITY);
        }
        return nInitialCapacity;
    }

    public float getLoadFactory4ConcurrentHashMap() {
        float fLoadFactory;
        try {
            fLoadFactory = Float.parseFloat(prop.getProperty(PROP_CHM_LOAD_FACTOR, DEF_VALUE_CHM_LOAD_FACTOR));
        } catch (NumberFormatException ex) {
            fLoadFactory = Float.parseFloat(DEF_VALUE_CHM_LOAD_FACTOR);
        }
        return fLoadFactory;
    }

    public int getConcurrencyLevel4ConcurrentHashMap() {
        int nConcurrencyLevel;
        try {
            nConcurrencyLevel = Integer.parseInt(prop.getProperty(PROP_CHM_CONCURRENCY_LEVEL, DEF_VALUE_CHM_CONCURRENCY_LEVEL));
        } catch (NumberFormatException ex) {
            nConcurrencyLevel = Integer.parseInt(DEF_VALUE_CHM_CONCURRENCY_LEVEL);
        }
        return nConcurrencyLevel;
    }
}
